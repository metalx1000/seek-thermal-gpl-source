Company = Seek Thermal, Inc.
Product = Reveal Pro
Model = RQ-FFIX
Serial Number = 1C18C0YWZLC8
Software = VERSION 2.3.0.0
Build = 202103261737
Firmware = 6
Language Support = EN, ES, FR, DE, KO, JP, CH, NL, IT, PT, PO

Subject to U.S. EAR Export Regulations