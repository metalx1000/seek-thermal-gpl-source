
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/fb.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include <linux/kthread.h>
#include <linux/mutex.h>
#include "lcd-driver.h"



#include "logo.c"


static struct fb_fix_screeninfo lcdfb_fix = {
	.id		      = "LCDFB",
	.type		      = FB_TYPE_PACKED_PIXELS,
	.visual		   = FB_VISUAL_TRUECOLOR,
	.xpanstep	   = 0,
	.ypanstep	   = 0,
	.ywrapstep	   = 0,
	.line_length   = FRAMEWIDTH*FRAMEBPP,
	.accel		   = FB_ACCEL_NONE,
};

static struct fb_var_screeninfo lcdfb_var = {
	.xres		         = FRAMEWIDTH,
	.yres		         = FRAMEHEIGHT,
	.xres_virtual	   = FRAMEWIDTH,
	.yres_virtual	   = FRAMEHEIGHT,
	.bits_per_pixel	= FRAMEBPP*8,
};

struct lcdfb_par {
	struct fb_info    *front_info;
	struct fb_info    *back_info;

   u32*              front;
   u32*              back;
   u16*              pong;
      
   struct task_struct   *worker_thread   
};

int tyrian_lcd_thread(void *data)
{
   struct lcdfb_par  *par=(struct lcdfb_par*)data;
   long l;
   int   red_offset  = 16;
   int   red_length  = 8;
   int   green_offset= 8;
   int   green_length= 8;
   int   blue_offset = 0;
   int   blue_length = 8;
      
   while(!kthread_should_stop()){
      
         
/* Alpha blend and convert to 565 */

      for(l=0;l<FRAMEWIDTH*FRAMEHEIGHT;++l){
         u16   r,g,b;
         u32   p0=par->back[l];
         u32   p1=par->front[l];
         u16   a=(p1>>24);
         
         
/* full back */
         if(a==0xff){
            r=(p0    ) & (0xff);
            g=(p0>>8 ) & (0xff);
            b=(p0>>16) & (0xff);         
         } else {
            r=((u16)((a)*((p0    )&0x00ff))+(u16)((256-a)*((p1>>16)&0x00ff)))>>8;
            g=((u16)((a)*((p0>>8 )&0x00ff))+(u16)((256-a)*((p1>>8 )&0x00ff)))>>8;
            b=((u16)((a)*((p0>>16)&0x00ff))+(u16)((256-a)*((p1    )&0x00ff)))>>8;         
         }
         
         par->pong[l]=((r>>3)<<11)|((g>>2)<<5)|((b>>3));//(r<<11)|(g<<5)|(b);
      }
      msleep(25);      
      tyrian_lcd_write(par->pong,FRAMEWIDTH*FRAMEHEIGHT);
      msleep(25);

   }
   return 0;
}

void lcdfb_fillrect(struct fb_info *info, const struct fb_fillrect *rect)
{
   sys_fillrect(info, rect);
}

void lcdfb_copyarea(struct fb_info *info, const struct fb_copyarea *area)
{
	sys_copyarea(info, area);
}

void lcdfb_imageblit(struct fb_info *info, const struct fb_image *image)
{
	sys_imageblit(info, image);
}

int lcdfb_ioctl(struct fb_info *info, unsigned int cmd, unsigned long arg)
{
   int retval = -ENOMEM;
//   struct lcdfb_par *par = info->par;
   switch (cmd) {

      default: 
      break;
   }

   return retval;
}

static ssize_t lcdfb_write(struct fb_info *info, const char __user *buf, size_t count, loff_t *ppos)
{
	struct lcdfb_par *par = info->par;
	unsigned long p = *ppos;
	void *dst;
	int err = 0;

   if (info->state != FBINFO_STATE_RUNNING){
      return -EPERM;
   }

   if (p > FRAMESIZE){
      return -EFBIG;
   }

   if (count > FRAMESIZE) {
      err = -EFBIG;
      count = FRAMESIZE;
   }

   if (count + p > FRAMESIZE) {
      if (!err){
         err = -ENOSPC;
      }
      count = FRAMESIZE - p;
   }

	dst = (void __force *) (info->screen_base + p);

	if (copy_from_user(dst, buf, count)){
		err = -EFAULT;
   }
	if(0)if  (!err){
		*ppos += count;
   }

	return (err) ? err : count;
}

static struct fb_ops lcdfb_ops = {
   .owner         = THIS_MODULE,
   .fb_read       = fb_sys_read,
   .fb_write      = lcdfb_write,
   .fb_fillrect   = lcdfb_fillrect,
   .fb_copyarea   = lcdfb_copyarea,
   .fb_imageblit  = lcdfb_imageblit,
   .fb_ioctl      = lcdfb_ioctl
};

static int lcdfb_probe(struct platform_device *dev)
{
	struct fb_info*   info;
	int               retval = -ENOMEM;   
   u8*               buf_mem=NULL;
   
   struct lcdfb_par* par=NULL;
   
   
/* Allocate first framebuffer */   
	info = framebuffer_alloc(sizeof(struct lcdfb_par), &dev->dev);
	if (!info){
      printk("Couldn't allocate framebuffer\n");
		goto err;
   }
   
	par = info->par;
	par->front_info = info;   


/* Video memory for both framebuffers */   
	buf_mem = (u16 *)kzalloc(3*FRAMESIZE, GFP_KERNEL);
	if (!buf_mem){
      printk("Couldn't allocate videomemory\n");
		goto err;
   }   

   memcpy(buf_mem,gimp_image.pixel_data,FRAMEWIDTH*FRAMEHEIGHT*sizeof(u32));

   par->front=buf_mem;
   par->back=(buf_mem+(FRAMESIZE));
   par->pong=(buf_mem+(FRAMESIZE*2));
   
	info->screen_base       = par->front;
	info->fbops             = &lcdfb_ops;
	info->fix               = lcdfb_fix;
	info->fix.smem_start    = virt_to_phys(par->front);
	info->fix.smem_len      = FRAMESIZE;
	info->var               = lcdfb_var;
	info->var.red.offset    = 16;
	info->var.red.length    = 8;
	info->var.green.offset  = 8;
	info->var.green.length  = 8;
	info->var.blue.offset   = 0;
	info->var.blue.length   = 8;
	info->var.transp.offset = 24;
	info->var.transp.length = 8;
	info->flags             = FBINFO_FLAG_DEFAULT | FBINFO_VIRTFB;
   
	retval = register_framebuffer(info);
	if (retval < 0){
      printk("Couldn't register first framebuffer\n");
		goto err1;
   }
      
	info = framebuffer_alloc(sizeof(struct lcdfb_par), &dev->dev);
	if (!info){
      printk("Couldn't allocate framebuffer\n");
		goto err;
   }
   
	par->back_info = info;    
   
	info->screen_base       = par->back;
	info->fbops             = &lcdfb_ops;
	info->fix               = lcdfb_fix;
	info->fix.smem_start    = virt_to_phys(par->back);
	info->fix.smem_len      = FRAMESIZE;
	info->var               = lcdfb_var;
	info->var.red.offset    = 16;
	info->var.red.length    = 8;
	info->var.green.offset  = 8;
	info->var.green.length  = 8;
	info->var.blue.offset   = 0;
	info->var.blue.length   = 8;
	info->var.transp.offset = 24;
	info->var.transp.length = 8;
	info->flags             = FBINFO_FLAG_DEFAULT | FBINFO_VIRTFB;
   
	retval = register_framebuffer(info);
	if (retval < 0){
      printk("Couldn't register second framebuffer\n");
		goto err1;
   }
   
   
	platform_set_drvdata(dev, par);

   retval = tyrian_lcd_open();
   if(retval<0){
      printk("Couldn't open LCD\n");
		goto err1;
   }

   par->worker_thread = kthread_run(tyrian_lcd_thread,(void*)par, "Tyrian LCD");

	printk(KERN_INFO"fb%d: Virtual frame buffer device, using %ldK of video memory\n",info->node, (2*FRAMESIZE) >> 10);
	return 0;

err1:
	framebuffer_release(info);
err:

   if(buf_mem){
      kfree(buf_mem);
   }
	return retval;
}


static int lcdfb_remove(struct platform_device *dev)
{
	struct lcdfb_par* par = platform_get_drvdata(dev);

	if (par) {

		unregister_framebuffer(par->front_info);
		unregister_framebuffer(par->back_info);

      kthread_stop(par->worker_thread);
      msleep(100);
      if (par->front){
         kfree(par->front);
      }
      
      tyrian_lcd_release();
		framebuffer_release(par->back_info);      
		framebuffer_release(par->front_info);
	}
	return 0;
}


static struct platform_driver lcdfb_driver = {
   .probe   = lcdfb_probe,
   .remove  = lcdfb_remove,
   .driver  = {
      .name	= "lcdfb",
   },
};

static struct platform_device *lcdfb_device;

static int __init lcdfb_init(void)
{
	int ret = 0;

   printk("Init\n");
	ret = platform_driver_register(&lcdfb_driver);

	if (!ret) {
      
		lcdfb_device = platform_device_alloc("lcdfb", 0);

		if (lcdfb_device){
			ret = platform_device_add(lcdfb_device);
		}else{
			ret = -ENOMEM;
      }
		if (ret) {
			platform_device_put(lcdfb_device);
			platform_driver_unregister(&lcdfb_driver);
		}
	}

	return ret;
}

static void __exit lcdfb_exit(void)
{
	platform_device_unregister(lcdfb_device);
	platform_driver_unregister(&lcdfb_driver);
}

module_init(lcdfb_init);
module_exit(lcdfb_exit);

MODULE_DESCRIPTION("LCD FB driver");
MODULE_AUTHOR("Andreas Engberg <aengberg@thermal.com");
MODULE_LICENSE("GPL");
