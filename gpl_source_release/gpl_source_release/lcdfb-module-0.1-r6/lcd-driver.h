#if !defined(AEG_LCD_DRIVER)
#define AEG_LCD_DRIVER

#define FRAMEWIDTH   320
#define FRAMEHEIGHT  240
#define FRAMEBPP     4
#define FRAMESIZE	   (FRAMEWIDTH*FRAMEHEIGHT*(FRAMEBPP))

extern int tyrian_lcd_open(void);
extern int tyrian_lcd_release(void);
extern int tyrian_lcd_write(u16* buf, int count);
 

typedef enum LCD_IL9341_Command {
	// Regulative Command Set
	LCD_Command_NoOperation = 0x00,
	LCD_Command_SoftwareReset = 0x01,
	LCD_Command_DisplayIdentification = 0x04,
	LCD_Command_DisplayStatus = 0x09,
	LCD_Command_DisplayPowerMode = 0x0A,
	LCD_Command_DisplayMADCTL = 0x0B,
	LCD_Command_DisplayPixelFormat = 0x0C,
	LCD_Command_DisplayImageFormat = 0x0D,
	LCD_Command_DisplaySignalMode = 0x0E,
	LCD_Command_DisplaySelfDiagnosticResult = 0x0F,
	LCD_Command_EnterSleepMode = 0x10,
	LCD_Command_SleepOut = 0x11,
	LCD_Command_PartialModeOn = 0x12,
	LCD_Command_NormalDisplayModeOn = 0x13,
	LCD_Command_DisplayInversionOff = 0x20,
	LCD_Command_DisplayInversionOn = 0x21,
	LCD_Command_GammaSet = 0x26,
	LCD_Command_DisplayOff = 0x28,
	LCD_Command_DisplayOn = 0x29,
	LCD_Command_ColumnAddressSet = 0x2A,
	LCD_Command_PageAddressSet = 0x2B,
	LCD_Command_MemoryWrite = 0x2C,
	LCD_Command_ColorSet = 0x02D,
	LCD_Command_MemoryRead = 0x2E,
	LCD_Command_PartialArea = 0x30,
	LCD_Command_VeritcalScrollingDefinition = 0x33,
	LCD_Command_TearingEffectLineOff = 0x34,
	LCD_Command_TearingEffectLineOn = 0x35,
	LCD_Command_MemoryAccessControl = 0x36,
	LCD_Command_VerticalScrollingStartAddress = 0x37,
	LCD_Command_IdleModeOff = 0x38,
	LCD_Command_IdleModeOn = 0x39,
	LCD_Command_PixelFormatSet = 0x3A,
	LCD_Command_WriteMemoryContinue = 0x3C,
	LCD_Command_ReadMemoryContinue = 0x3E,
	LCD_Command_SetTearScanline = 0x44,
	LCD_Command_GetScanLine = 0x45,
	LCD_Command_WriteDisplayBrightness = 0x51,
	LCD_Command_ReadDisplayBrightness = 0x52,
	LCD_Command_WriteCTRLDisplay = 0x53,
	LCD_Command_ReadCTRLDisplay = 0x54,
	LCD_Command_WriteControlAdaptiveBrightnessControl = 0x55,
	LCD_Command_ReadControlAdaptiveBrightnessControl = 0x56,
	LCD_Command_WriteCABCMinimumBrightness = 0x5E,
	LCD_Command_ReadCABCMinimumBrightness = 0x5F,
	LCD_Command_ReadID1 = 0xDA,
	LCD_Command_ReadID2 = 0xDB,
	LCD_Command_ReadID3 = 0xDC,

	// Extended Command Set
	LCD_Command_RGBInterfaceSignalControl = 0xB0,
	LCD_Command_NormalModeFrameControl = 0xB1,
	LCD_Command_IdleModeFrameControl = 0xB2,
	LCD_Command_PartialModeFrameControl = 0xB3,
	LCD_Command_DisplayInversionControl = 0xB4,
	LCD_Command_BlankingPorchControl = 0xB5,
	LCD_Command_DisplayFunctionControl = 0xB6,
	LCD_Command_EntryModeSet = 0xB7,
	LCD_Command_BacklightControl1 = 0xB8,
	LCD_Command_BacklightControl2 = 0xB9,
	LCD_Command_BacklightControl3 = 0xBA,
	LCD_Command_BacklightControl4 = 0xBB,
	LCD_Command_BacklightControl5 = 0xBC,
	LCD_Command_BacklightControl6 = 0xBD,
	LCD_Command_BacklightControl7 = 0xBE,
	LCD_Command_BacklightControl8 = 0xBF,
	LCD_Command_PowerControl1 = 0xC0,
	LCD_Command_PowerControl2 = 0xC1,
	LCD_Command_VCOMControl1 = 0xC5,
	LCD_Command_VCOMControl2 = 0xC7,
	LCD_Command_NVMemoryWrite = 0xD0,
	LCD_Command_NVMemoryProtectionKey = 0xD1,
	LCD_Command_NVMemoryStatusRead = 0xD2,
	LCD_Command_ReadID4 = 0xD3,
	LCD_Command_PositiveGammaCorrection = 0xE0,
	LCD_Command_NegativeGammaCorrection = 0xE1,
	LCD_Command_DigitalGammaControl1 = 0xE2,
	LCD_Command_DigitalGammaControl2 = 0xE3,
	LCD_Command_InterfaceControl = 0xF6,
} LCD_IL9341_Commands;

#endif
