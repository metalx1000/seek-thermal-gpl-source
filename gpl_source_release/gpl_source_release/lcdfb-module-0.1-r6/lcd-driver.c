
#include <linux/mm.h>
#include <linux/miscdevice.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/mman.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <linux/fs.h>
#include <linux/fb.h>
#include <linux/gpio.h>

#include "lcd-driver.h"


#define GPMC_BASE_OFFSET   0x50000000
#define GPMC_BASE_SIZE     0x01000000

#define LCD_BASE_OFFSET    0x01000000
#define LCD_BASE_SIZE      0x01000000

#define GPIO_BASE_SIZE     0x00001000


#define GPIO0_BASE_OFFSET  0x44E07000
#define GPIO1_BASE_OFFSET  0x4804C000
#define GPIO2_BASE_OFFSET  0x481ac000


volatile u32 *gpio0_BaseAddr  = NULL;
volatile u32 *gpio0_SetAddr   = NULL;
volatile u32 *gpio0_ClearAddr = NULL;
volatile u32 *gpio0_DirAddr   = NULL;

volatile u32 *gpio1_BaseAddr  = NULL;
volatile u32 *gpio1_SetAddr   = NULL;
volatile u32 *gpio1_ClearAddr = NULL;
volatile u32 *gpio1_DirAddr   = NULL;

#define GPIO_LCD_RSTn      0x00001000  // GPIO0_12
#define GPIO_LCD_DCx       0x00200000  // GPIO1_21

#define PIN_LCD_RSTn       (12)
#define PIN_LCD_DCx        (53)

volatile uint16_t *lcdMem = NULL;

int pin_lcd_rstn  = 0;
int pin_lcd_dcx   = 0;

struct lcdinfo
{
   int major;
   
//   struct cdev cdev;   
} lcdinfo;




static void WMLCDCOM(uint16_t arg)
{
   *gpio1_ClearAddr = GPIO_LCD_DCx;   
//   udelay(5);
	*lcdMem = arg;
   ndelay(1);

}
static void WMLCDDATA(uint16_t arg)
{
   *gpio1_SetAddr = GPIO_LCD_DCx;
//   udelay(5);
	*lcdMem = arg;
   ndelay(1);

}
static uint16_t WMLCDGET(void)
{
   *gpio1_SetAddr = GPIO_LCD_DCx; 
//   udelay(5);
	return *lcdMem;//*(lcdMem|0x800);
}


static void WMLCDWR(u16 *buffer,int count)
{
   *gpio1_SetAddr = GPIO_LCD_DCx; 
   while(count){
   	//*(lcdMem|0x800)= *buffer;  
 //     udelay(5);
      *(lcdMem)= *buffer;      
//      ndelay(1);
      buffer++;
      count--;
   }
   ndelay(5);
}
static void WMLCDRD(u16 *buffer,int count)
{
   *gpio1_SetAddr = GPIO_LCD_DCx; 

   while(count){
   	*buffer=*(lcdMem);      
      ndelay(1);

      buffer++;
      count--;
   }
}


static void tyrian_lcd_init(void)
{
   mdelay(1);
   *gpio0_ClearAddr = GPIO_LCD_RSTn; 

   mdelay(20);
   *gpio0_SetAddr = GPIO_LCD_RSTn; 
   mdelay(50);
 
   WMLCDCOM(0x00);
   WMLCDCOM(0x00);
   WMLCDCOM(0x00);
   WMLCDCOM(0x00);
 
   WMLCDCOM(LCD_Command_SoftwareReset);
   mdelay(120);
     
   WMLCDCOM(LCD_Command_DisplayOff);

   WMLCDCOM(0xf7);		// Defined by display provider
   WMLCDDATA(0x20);
 
   WMLCDCOM(0xea);		// Defined by display provider
   WMLCDDATA(0x00);
   WMLCDDATA(0x00);

   WMLCDCOM(LCD_Command_PowerControl1);
   WMLCDDATA(0x26);    //VRH: 4.75V

   WMLCDCOM(LCD_Command_PowerControl2);
   WMLCDDATA(0x11);    //AVDD: VCIx2, VGH: VCIx7, VGL: -VCIx3

   WMLCDCOM(LCD_Command_VCOMControl1);
   WMLCDDATA(0x35);    //4.025V VCOMH
   WMLCDDATA(0x3e);    //-0.95V VCOML

   WMLCDCOM(LCD_Command_VCOMControl2);
   WMLCDDATA(0xbe);    //VMF: VMH�2, VML�2

   WMLCDCOM(LCD_Command_MemoryAccessControl);
   WMLCDDATA(0x08);    //	BGR->RGB

   WMLCDCOM(LCD_Command_PixelFormatSet);
   WMLCDDATA(0x55);    //	16 bit per pixel

   WMLCDCOM(LCD_Command_NormalModeFrameControl);	// Frame rate control
   WMLCDDATA(0x00);    // Division ratio for internal clocks: Fosc / 1
   WMLCDDATA(0x1B);    // 70 Hz

   WMLCDCOM(LCD_Command_GammaSet);
   WMLCDDATA(0x01);	// Gamma Curve 1

   WMLCDCOM(LCD_Command_SleepOut); //Sleep OUT
   mdelay(100);

   WMLCDCOM(LCD_Command_DisplayOn); //Display ON
   mdelay(100);
    
}


void tyrian_lcd_blit(u16 *data,int count)
{
   if(count > FRAMEWIDTH*FRAMEHEIGHT){
      count = FRAMEWIDTH*FRAMEHEIGHT;
   }
   WMLCDCOM(LCD_Command_MemoryAccessControl);
   WMLCDDATA(0xE0);

   WMLCDCOM(LCD_Command_ColumnAddressSet);
   WMLCDDATA(0x00);
   WMLCDDATA(0x00);
   WMLCDDATA(0x01);
   WMLCDDATA(0x3f);

   WMLCDCOM(LCD_Command_PageAddressSet);
   WMLCDDATA(0x00);
   WMLCDDATA(0x00);
   WMLCDDATA(0x00);
   WMLCDDATA(0xef);

   WMLCDCOM(LCD_Command_MemoryWrite);
   WMLCDWR((u16*)data,count);

   WMLCDCOM(LCD_Command_MemoryAccessControl);
   WMLCDDATA(0x40);
   
}

void tyrian_lcd_clear(u16 color,int count)
{
   WMLCDCOM(LCD_Command_MemoryAccessControl);
   WMLCDDATA(0xE0);

   WMLCDCOM(LCD_Command_ColumnAddressSet);
   WMLCDDATA(0x00);
   WMLCDDATA(0x00);
   WMLCDDATA(0x01);
   WMLCDDATA(0x3f);

   WMLCDCOM(LCD_Command_PageAddressSet);
   WMLCDDATA(0x00);
   WMLCDDATA(0x00);
   WMLCDDATA(0x00);
   WMLCDDATA(0xef);

   WMLCDCOM(LCD_Command_MemoryWrite);

   while(count){
      WMLCDDATA(color);
      count--;
   }

   WMLCDCOM(LCD_Command_MemoryAccessControl);
   WMLCDDATA(0x40);
   
}




int tyrian_lcd_write(u16* buf, int count)
{
   static int updates=0;
   if(!buf){
      return -ENOMEM;
   }
   if(count > (FRAMEHEIGHT*FRAMEWIDTH)){
      count=FRAMEHEIGHT*FRAMEWIDTH;
   }
   
   tyrian_lcd_blit(buf,count);     
   
   return 0;
}

int tyrian_lcd_open(void) {
   u32 *gpmc=ioremap_nocache(GPMC_BASE_OFFSET,GPMC_BASE_SIZE);
   int i;
   u16 d[10];

   if(!gpmc){
      printk("Error gpmc\n");      
      return -ENOMEM;
   }
   
/* Set up GPMC CS0 for LCD */	
/* Disable CS0 */
   gpmc[0x1e]=0x00000000;
/* Config 1 - No burst, async, 16-bit, non multiplexed */
   gpmc[0x18]=0x50001000;
/* Config 2 -Assert CS on fclk 0, deassert CS on fclk 1 */
   gpmc[0x19]=0x00040400;
/* Config 3 -Unused ADV/ALE */
   gpmc[0x1a]=0x00000000;     
/* Config 4 -Assert WE & OE on fclk 0, deassert OE on fclk 1 */
   gpmc[0x1b]=0x03000300;   
/* Config 5 -Data valid on fclk 0, cycle time 1 fclks */
   gpmc[0x1c]=0x00030404;   
/* Config 6 -No back to back cycle restrictions */
   gpmc[0x1d]=0x00000000;    
/* Config 7 -CS0: Set base address 0x01000000, 16MB region, and enable CS */
   gpmc[0x1e]=0x00000f41;  

   iounmap(gpmc);
   gpmc=NULL;
   
   gpio_request(PIN_LCD_RSTn,"PIN LCD RSTn");
   gpio_request(PIN_LCD_DCx,"PIN LCD DCx");

   gpio0_BaseAddr = ioremap_nocache(GPIO0_BASE_OFFSET, GPIO_BASE_SIZE);
   if(!gpio0_BaseAddr){
      printk("Error gpio0\n");
      return -ENOMEM;
   }

   gpio0_SetAddr    = &gpio0_BaseAddr[0x194>>2];
   gpio0_ClearAddr  = &gpio0_BaseAddr[0x190>>2];
   gpio0_DirAddr    = &gpio0_BaseAddr[0x134>>2];
	
/* Set RSTn to output */    
   *gpio0_DirAddr   &=~(GPIO_LCD_RSTn);
 
   
   gpio1_BaseAddr = ioremap_nocache(GPIO1_BASE_OFFSET, GPIO_BASE_SIZE);
   if(!gpio1_BaseAddr){
      printk("Error gpio1\n");
      return -ENOMEM;
   }

   gpio1_SetAddr    = &gpio1_BaseAddr[0x194>>2];
   gpio1_ClearAddr  = &gpio1_BaseAddr[0x190>>2];
   gpio1_DirAddr    = &gpio1_BaseAddr[0x134>>2];

/* Set DCx to output */    
   *gpio1_DirAddr   &=~(GPIO_LCD_DCx);
  
/* Reset low */
   *gpio0_ClearAddr = GPIO_LCD_RSTn;
 

   lcdMem = ioremap_nocache(LCD_BASE_OFFSET, LCD_BASE_SIZE);
   if(!lcdMem){
      printk("Error lcd\n");      
      return -ENOMEM;
   }
    
   tyrian_lcd_init(); 

//   tyrian_lcd_clear(0x0,FRAMEWIDTH*FRAMEHEIGHT);
//   tyrian_lcd_blit(gimp_image.pixel_data,FRAMEWIDTH*FRAMEHEIGHT);

   WMLCDCOM(LCD_Command_DisplayIdentification);
   WMLCDRD(d,4);
   printk("0x%04x 0x%04x 0x%04x 0x%04x\n",d[0],d[1],d[2],d[3]);
   
   WMLCDCOM(LCD_Command_DisplayStatus);
   WMLCDRD(d,5);
   printk("0x%04x 0x%04x 0x%04x 0x%04x 0x%04x\n",d[0],d[1],d[2],d[3],d[4]);   

   WMLCDCOM(LCD_Command_DisplayPowerMode);
   WMLCDRD(d,2);
   printk("0x%04x 0x%04x\n",d[0],d[1]);     

   WMLCDCOM(LCD_Command_DisplayPixelFormat);
   WMLCDRD(d,2);
   printk("0x%04x 0x%04x\n",d[0],d[1]);    

   WMLCDCOM(LCD_Command_DisplayImageFormat);
   WMLCDRD(d,2);
   printk("0x%04x 0x%04x\n",d[0],d[1]);  


   return 0;
}

int tyrian_lcd_release(void) {

   if(lcdMem){
      iounmap(lcdMem);
      lcdMem = NULL;
   }

   gpio_free(PIN_LCD_RSTn);
   gpio_free(PIN_LCD_DCx);
      
   if(gpio0_BaseAddr){
      iounmap(gpio0_BaseAddr);
         
      gpio0_BaseAddr  = NULL;
      gpio0_SetAddr   = NULL;
      gpio0_ClearAddr = NULL;
      gpio0_DirAddr   = NULL;
   }   
   if(gpio1_BaseAddr){
      iounmap(gpio1_BaseAddr);
         
      gpio1_BaseAddr  = NULL;
      gpio1_SetAddr   = NULL;
      gpio1_ClearAddr = NULL;
      gpio1_DirAddr   = NULL;
   }
   return 0;
}
